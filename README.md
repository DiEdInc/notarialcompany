#NotarialCompany

Notarial Company project for the university using .NET (WPF, ADO.NET) with Microsoft SQL Server. 
## DB creation
DB first approach used.
## Installation
1. Just execute SQL scripts from the project NotarialCompanyDatabase
2. Start program and enjoy
## Usage
### Clients, Deals, Employees and their Positions, Services, Users page
CRUD operations on them depending on the permissions.
### Login Page
Enter your login and password and click the login button.
## License
Of couse Open-Source license